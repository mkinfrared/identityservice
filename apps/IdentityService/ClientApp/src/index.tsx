import "@identity-service/ui/lib/esm/styles.min.css";
import ReactDOM from "react-dom";

import App from "./App";

import "./index.scss";

ReactDOM.render(<App />, document.getElementById("app"));
